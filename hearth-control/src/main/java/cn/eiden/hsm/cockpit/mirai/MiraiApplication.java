package cn.eiden.hsm.cockpit.mirai;


import cn.eiden.hsm.cockpit.mirai.events.GroupEvent;
import cn.eiden.hsm.game.card.CardFactory;
import net.mamoe.mirai.Bot;
import net.mamoe.mirai.BotFactory;
import net.mamoe.mirai.event.Events;
import net.mamoe.mirai.utils.BotConfiguration;
import net.mamoe.mirai.utils.DeviceInfo;

import java.io.File;

/**
 * @author Eiden J.P Zhou
 * @date 2020/8/4 17:02
 */
public class MiraiApplication {
    public static void main(String[] args) {
//        Bot bot = BotFactory.INSTANCE.newBot(1L, "");
        // 使用新的初始化方式以规避QQ安全检查
        // 使用方式见如下链接
        // https://mirai.mamoe.net/topic/223/%E6%97%A0%E6%B3%95%E7%99%BB%E5%BD%95%E7%9A%84%E4%B8%B4%E6%97%B6%E5%A4%84%E7%90%86%E6%96%B9%E6%A1%88
        BotConfiguration botConfiguration = new BotConfiguration();
        botConfiguration.fileBasedDeviceInfo("E://device.json");
        Bot bot = BotFactory.INSTANCE.newBot(1L, "pwd",botConfiguration);
        bot.login();
        //输出好友
        bot.getFriends().forEach(friend -> System.out.println(friend.getId() + ":" + friend.getNick()));
        bot.getEventChannel().registerListenerHost(new GroupEvent());
        CardFactory.getInstance();
        System.out.println();
        System.out.println("系统启动完成！");

        //阻塞当前线程
        bot.join();
    }
}
